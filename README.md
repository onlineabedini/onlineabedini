# `onlineabedini`
**`International Web Developer`**  |   **`Javascript`**  |  **`Full Stack`**  | **`Software engineering expert`** 

------------

- current position: Tractive Sdn Bhd
- **Services:**
	- Web Application Develop
	- Cloud Base Development
	- Social Bot  Development
	- Engnearing and Develop teach

------------
> I work based on results, that's what makes my work unique! In a profitable and valuable project, it is enough for me to specify the goal in order to advance the project and reach the goal in a documentary way and like a documentary maker!

**من بر اساس نتیجه کار میکنم!**
> من بر اساس نتیجه کار می کنم، این چیزی است که  کار من را منحصر به فرد می کند! کافی است در یک پروژه پر سود و ارزشمند هدف برای من مشخص شود تا به صورت مستند و مانند یک مستند ساز پروژه را پیش ببرم و به هدف برسم!

### Resume and Samples
[`Public resume ( For the private version ask me )`](https://github.com/onlineabedini/public-resume "`Public resume ( For the private version ask me )`") [ Click Here](https://github.com/onlineabedini/public-resume " Click Here")

[`Project Samples` ](https://github.com/onlineabedini?tab=repositories "`Project Samples` ") [Click Here](https://github.com/onlineabedini?tab=repositories "Click Here")

------------
## onlineabedini npm  
> Install my package to develop easily

`$ npm install onlineabedini`

------------


### Contact me:

### Social Links
[`Instagram` Click Here](https://www.instagram.com/onlineabedini/?hl=en "`Instagram` Click Here") |
[`Telegram` Click Here](https://telegram.me/Onlineabedini "`Telegram` Click Here") |
[`Whatsapp` Click Here](https://wa.me/989367312702 "`Whatsapp` Click Here") |
[`Linkedin` Click Here](https://www.linkedin.com/in/onlineabedini/ "`Linkedin` Click Here")

**Phone:**
	`0098-936-731-2702` | `0098-992-473-0751`

**Mail:**
	`onlineabedini@gmail.com`

**Social Id:**
	`@onlineabedini`
